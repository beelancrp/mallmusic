package com.nsofta.newmusicmallplayer.api.model;

import com.google.gson.annotations.SerializedName;
import com.nsofta.newmusicmallplayer.player.model.MallInfo;

public class ObjectResponse extends BaseResponse {

    @SerializedName("object")
    public MallInfo mallInfo;

}
