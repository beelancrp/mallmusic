package com.nsofta.newmusicmallplayer.api;

import com.nsofta.newmusicmallplayer.api.model.BaseResponse;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MallRestService {

    public static final String URL = "http://m.musicmall.com.ua";
    private static MallApi mApi;

    private static Interceptor authorizationInterceptor = chain -> {
        Request original = chain.request();
        Request request = original.newBuilder()
                .header("Authorization", "Basic d2ViX3VzZXI6d2ViMWNtdXNpY21hbGw=")
                .method(original.method(), original.body())
                .build();
        return chain.proceed(request);
    };

    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(authorizationInterceptor)
            .build();

    private static Retrofit serviceCreator = new Retrofit.Builder()
            .baseUrl(URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static MallApi getApi() {
        if (mApi == null) {
            mApi = serviceCreator.create(MallApi.class);
        }
        return mApi;
    }

    public static Callback<BaseResponse> mDefaultResponse = new Callback<BaseResponse>() {
        @Override
        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

        }

        @Override
        public void onFailure(Call<BaseResponse> call, Throwable t) {

        }
    };

}
