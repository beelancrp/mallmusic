package com.nsofta.newmusicmallplayer.api.model;

import com.google.gson.annotations.SerializedName;
import com.nsofta.newmusicmallplayer.player.model.Ad;

import java.util.List;

public class AdResponse extends BaseResponse {
    @SerializedName("adv")
    public List<Ad> ads;
}
