package com.nsofta.newmusicmallplayer.api.model;

import com.google.gson.annotations.SerializedName;
import com.nsofta.newmusicmallplayer.player.model.Channel;

import java.util.List;

public class MusicResponse extends BaseResponse {

    @SerializedName("channels")
    public List<Channel> channels;

}
