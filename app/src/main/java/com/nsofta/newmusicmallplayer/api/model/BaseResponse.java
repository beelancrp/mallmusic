package com.nsofta.newmusicmallplayer.api.model;

import android.support.annotation.CallSuper;

public class BaseResponse {
    public int code;
    public String message;

    @CallSuper
    public boolean invalidate() {
        return code == 1;
    }
}
