package com.nsofta.newmusicmallplayer.api;

import com.nsofta.newmusicmallplayer.api.model.AdResponse;
import com.nsofta.newmusicmallplayer.api.model.BaseResponse;
import com.nsofta.newmusicmallplayer.api.model.MusicResponse;
import com.nsofta.newmusicmallplayer.api.model.ObjectResponse;

import org.json.JSONArray;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MallApi {

    @GET("/test_v2/hs/setting/music")
    Call<MusicResponse> getMusic(@Query("id") int id);

    @GET("/test_v2/hs/setting/adv")
    Call<AdResponse> getAds(@Query("id") int id);

    @GET("/test_v2/hs/setting/object")
    Call<ObjectResponse> getObject(@Query("id") int id);

    @FormUrlEncoded
    @POST("/test_v2/hs/setting/logs")
    Call<BaseResponse> sendLogs(@Field("key") String key,
                                @Field("value") Object event);

    @FormUrlEncoded
    @POST("/test_v2/hs/setting/logs")
    Call<BaseResponse> sendLogs(@FieldMap Map<String, Object> events);

    @GET("/test_v2/hs/setting/reg_token")
    Call<BaseResponse> sendToken(@Query("id") int id,
                                 @Query("token") String token);

    @POST
    @FormUrlEncoded
    Call<BaseResponse> blockTrack(@Field("id") int id, @Field("blocked_tracks") JSONArray tracks);

}
