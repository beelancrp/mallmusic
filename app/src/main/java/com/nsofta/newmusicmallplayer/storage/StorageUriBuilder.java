package com.nsofta.newmusicmallplayer.storage;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v4.provider.DocumentFile;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

public class StorageUriBuilder {

    public static DirUri fromDirUri(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            return fromDir(DocumentFile.fromTreeUri(context, uri));
        } else {
            return fromDir(DocumentFile.fromFile(new File(uri.getPath())));
        }
    }

    public static FileUri fromFileUri(Context context, Uri uri) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            return fromFile(DocumentFile.fromSingleUri(context, uri));
        } else {
            return fromFile(DocumentFile.fromFile(new File(uri.getPath())));
        }
    }

    public static FileUri fromFile(DocumentFile file) {
        return new FileUri(file);
    }

    public static DirUri fromDir(DocumentFile dir) {
        return new DirUri(dir);
    }


    public abstract static class ExternalUri {

        protected DocumentFile mFile;

        private ExternalUri(DocumentFile rootTree) {
            mFile = rootTree;
        }

        public DocumentFile getFile() {
            return mFile;
        }

        public boolean delete() {
            return mFile.delete();
        }

        public abstract boolean isFile();

        public abstract boolean isDirectory();

        public DirUri buildParent(Context context) {
            DocumentFile parent;
            //С учетом того, что файл выбран через Intent.ACTION_OPEN_DOCUMENT_TREE
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                parent = DocumentFile.fromTreeUri(context, mFile.getUri());
                String[] segments = mFile.getUri().toString()
                        .replace(parent.getUri().toString(), "")
                        .split("%2F");
                DirUri dir = fromDir(parent);
                for (int i = 0; i < segments.length - 1; i++) {
                    dir = dir.appendDirectory(segments[i]);
                }
                return dir;
            } else {
                File file = new File(mFile.getUri().getPath());
                return new DirUri(DocumentFile.fromFile(file.getParentFile()));
            }
        }
    }

    public static class DirUri extends ExternalUri {

        private DirUri(DocumentFile rootTree) {
            super(rootTree);
        }

        @Override
        public boolean isFile() {
            return false;
        }

        @Override
        public boolean isDirectory() {
            return true;
        }

        public DirUri appendDirectory(String name) {
            DocumentFile dir = mFile.findFile(name);
            if (dir == null || dir.isFile()) {
                dir = mFile.createDirectory(name);
            }
            return new DirUri(dir);
        }

        public FileUri appendFile(String name) {
            DocumentFile file = mFile.findFile(name);
            if (file == null || file.isDirectory()) {
                String title;
                if (name.lastIndexOf(".") > -1) {
                    title = name.substring(0, name.lastIndexOf("."));
                } else {
                    title = name;
                }
                file = mFile.createFile(getMimeType(name), title);
            }
            return new FileUri(file);
        }

        private String getMimeType(String name) {
            String type = null;
            String extension = MimeTypeMap.getFileExtensionFromUrl(name);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
            return type;
        }
    }

    public static class FileUri extends ExternalUri {

        private FileUri(DocumentFile file) {
            super(file);
        }

        @Override
        public boolean isFile() {
            return true;
        }

        @Override
        public boolean isDirectory() {
            return false;
        }

        public InputStream getInputStream(Context context) throws FileNotFoundException {
            return context.getContentResolver().openInputStream(mFile.getUri());
        }

        public OutputStream getOutputStream(Context context) throws FileNotFoundException {
            return context.getContentResolver().openOutputStream(mFile.getUri());
        }
    }

}
