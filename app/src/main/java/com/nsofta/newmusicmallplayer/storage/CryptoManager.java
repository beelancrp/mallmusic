package com.nsofta.newmusicmallplayer.storage;

import android.content.Context;
import android.support.v4.provider.DocumentFile;
import android.util.Log;

import com.facebook.android.crypto.keychain.AndroidConceal;
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.CryptoConfig;
import com.facebook.crypto.Entity;
import com.facebook.crypto.keychain.KeyChain;
import com.nsofta.newmusicmallplayer.logging.Logger;

import java.io.InputStream;
import java.io.OutputStream;

import okio.Okio;

import static com.nsofta.newmusicmallplayer.utils.StringUtils.generateString;

public class CryptoManager {

    public static final String LOG_TAG = CryptoManager.class.getSimpleName();

    private static CryptoManager sInstance;
    private Context mContext;
    private Crypto mCrypto;

    public static CryptoManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new CryptoManager(context);
        } else {
            sInstance.mContext = context;
        }
        return sInstance;
    }

    private CryptoManager(Context context) {
        KeyChain keyChain = new SharedPrefsBackedKeyChain(context, CryptoConfig.KEY_256);
        mCrypto = AndroidConceal.get().createDefaultCrypto(keyChain);
        mContext = context;
    }

    public DocumentFile encrypt(String name, DocumentFile rawFile) {
        if (!mCrypto.isAvailable()) {
            Logger.logError(LOG_TAG, new Exception("Crypto not available"));
            return null;
        }
        String randomName = generateString();
        Entity entity = Entity.create(name);
        StorageUriBuilder.FileUri finalUri = StorageUriBuilder
                .fromDir(rawFile.getParentFile())
                .appendFile(randomName);
        try {
            OutputStream outputStream = mCrypto.getCipherOutputStream(finalUri.getOutputStream(mContext), entity);
            Okio.buffer(Okio.sink(outputStream))
                    .writeAll(Okio.source(StorageUriBuilder.fromFile(rawFile).getInputStream(mContext)));
            return finalUri.getFile();
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
            return null;
        }
    }

    public DocumentFile decrypt(String name, DocumentFile encryptedFile) {
        if (!mCrypto.isAvailable()) {
            Logger.logError(LOG_TAG, new Exception("Crypto not available"));
            return null;
        }
        Entity entity = Entity.create(name);
        StorageUriBuilder.FileUri originalUri = StorageUriBuilder
                .fromFile(encryptedFile)
                .buildParent(mContext)
                .appendFile(name);
        try {
            InputStream inputStream = mCrypto.getCipherInputStream(StorageUriBuilder.fromFile(encryptedFile).getInputStream(mContext), entity);
            Okio.buffer(Okio.source(inputStream)).readAll(Okio.sink(originalUri.getOutputStream(mContext)));
            return originalUri.getFile();
        } catch (Exception e) {
            Logger.logError(LOG_TAG, e);
            if (originalUri != null) {
                return originalUri.getFile();
            }
            return null;
        }
    }
}
