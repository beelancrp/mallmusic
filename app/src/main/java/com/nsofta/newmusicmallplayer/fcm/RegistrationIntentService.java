package com.nsofta.newmusicmallplayer.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nsofta.newmusicmallplayer.api.MallRestService;
import com.nsofta.newmusicmallplayer.api.model.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Illya on 17.02.2017.
 */
public class RegistrationIntentService extends IntentService {
    private SharedPreferences sharedPreferences;

    public RegistrationIntentService(String name) {
        super(name);
    }

    public RegistrationIntentService() {
        super(RegistrationIntentService.class.getSimpleName());
    }
    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();
            Log.i("GCM", "Token: " + instanceID.getToken());
            if (TextUtils.isEmpty(instanceID.getToken()))
                return;
            sharedPreferences.edit().putString("push", instanceID.getToken()).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
