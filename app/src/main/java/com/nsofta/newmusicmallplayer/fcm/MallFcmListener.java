package com.nsofta.newmusicmallplayer.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.sync.SyncTask;
import com.nsofta.newmusicmallplayer.ui.activities.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Illya on 20.02.2017.
 */

public class MallFcmListener extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("Push", "RECEIVE:" + remoteMessage.getData().toString());

        String data = remoteMessage.getData().get("data");

        try {
            JSONObject obj = new JSONObject(data);
            Log.e("Push", "RECEIVE:" + obj.getString("connect_now"));
            Log.e("Push", "RECEIVE:" + obj.getString("send_log"));

            if (obj.has("connect_now")) {
//                boolean sync = obj.getBoolean("connect_now");
                if (obj.getBoolean("connect_now")) {
                    Intent intent = new Intent(this.getApplicationContext(), SyncTask.class);
                    intent.putExtra("start_sync", true);
                    startService(intent);
                }
            }
            if (obj.has("send_log")) {
                //TODO: Implement logic for sending log.
            }

        } catch (JSONException e) {
            Logger.logError(this.getClass().getSimpleName(), e);
        }
    }
}
