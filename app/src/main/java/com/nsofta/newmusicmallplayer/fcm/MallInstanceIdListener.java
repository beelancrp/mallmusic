package com.nsofta.newmusicmallplayer.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Illya on 20.02.2017.
 */

public class MallInstanceIdListener extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        Log.e("PUSH", "START SERVICE");
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
