package com.nsofta.newmusicmallplayer.player;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.provider.DocumentFile;

import com.nsofta.newmusicmallplayer.player.model.Ad;
import com.nsofta.newmusicmallplayer.storage.CryptoManager;
import com.nsofta.newmusicmallplayer.storage.StorageUriBuilder;

public class DecryptAdTask extends AsyncTask<Ad, Void, Uri> {

    private Context mContext;
    private CryptoManager mCryptoManager;

    public DecryptAdTask(Context context) {
        mContext = context;
        mCryptoManager = CryptoManager.getInstance(context);
    }

    @Override
    protected Uri doInBackground(Ad... params) {
        Ad ad = params[0];
        DocumentFile adFile = mCryptoManager.decrypt(ad.adName,
                StorageUriBuilder.fromFileUri(mContext, Uri.parse(ad.filePath)).getFile());
        return adFile.getUri();
    }
}
