package com.nsofta.newmusicmallplayer.player.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MallInfo {

    @SerializedName("object_name")
    @Expose
    public String name;
    @SerializedName("use_adv")
    @Expose
    public boolean useAds;
    @SerializedName("adv_fixed_time")
    @Expose
    public boolean advFixedTime;
    @SerializedName("corporate_edition")
    @Expose
    public boolean corporateEdition;
    @SerializedName("ftp")
    @Expose
    public String ftpHost;
    @SerializedName("ftp_login")
    @Expose
    public String ftpLogin;
    @SerializedName("ftp_pas")
    @Expose
    public String ftpPassword;
    @SerializedName("deys")
    @Expose
    public List<Day> days;
    @SerializedName("Blocked_tracks")
    @Expose
    public List<BlockedTrack> blockedTracks;
}
