package com.nsofta.newmusicmallplayer.player;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.nsofta.newmusicmallplayer.player.model.Ad;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by beeLAN on 24.02.2017.
 */

public class AdsManager {


    public static final String LOG_TAG = AdsManager.class.getName();
    private static AdsManager sInstance;
    private List<Ad> mAds = new LinkedList<>();
    private ArrayDeque<Ad> adsQueue = new ArrayDeque<>();

    private AdsManager() {
    }

    public static AdsManager getInstance() {
        if (sInstance == null) {
            sInstance = new AdsManager();
        }
        return sInstance;
    }

    public void setAdsList(List<Ad> list) {
        this.mAds.addAll(list);
    }

    @NonNull
    private List<Ad> getAdByIndex(int currentAdIndex) {
        List<Ad> ads = new ArrayList<>();
        for (Ad ad : mAds) {
            if (ad.getIndexMap().get(currentAdIndex))
                ads.add(ad);
        }
        return ads;
    }

    @Nullable
    public Ad findAdByIndex(int currentAdIndex) {
        List<Ad> tmp = getAdByIndex(currentAdIndex);

        if (tmp.isEmpty()) return null;

        if (tmp.size() == 1) return tmp.get(0);

        Collections.sort(tmp, (o1, o2) -> o1.adName.compareTo(o2.adName));
        adsQueue.addAll(tmp);
        return getTopStackAd();
    }

    @Nullable
    public Ad getTopStackAd() {
        if (isHasAdsStack()) return adsQueue.pop();
        return null;
    }

    public boolean isHasAdsStack() {
        return this.adsQueue.isEmpty();
    }

}
