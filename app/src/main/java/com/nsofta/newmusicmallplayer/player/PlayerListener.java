package com.nsofta.newmusicmallplayer.player;

import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.player.model.Track;

import java.util.List;

public interface PlayerListener {
    void onProgressChanged(int millis);

    void onTrackChanged(Track track);

    void onPlaylistChanged(List<Channel> channels);
}

