package com.nsofta.newmusicmallplayer.player.model;

import com.j256.ormlite.field.DatabaseField;

public class Track {

    public static class Columns {
        public static final String ID = "_id";
        public static final String TITLE = "title";
        public static final String PATH = "path";
        public static final String LENGTH = "length";
        public static final String CHANNEL = "channel";
    }

    @DatabaseField(generatedId = true, columnName = Columns.ID)
    public int id;
    @DatabaseField(columnName = Columns.TITLE)
    public String title;
    @DatabaseField(columnName = Columns.PATH)
    public String path;
    @DatabaseField(columnName = Columns.LENGTH)
    public int lengthMillis;
    @DatabaseField(columnName = Columns.CHANNEL, foreign = true, foreignAutoRefresh = true, canBeNull = false)
    public Channel channel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Track track = (Track) o;

        return path != null ? path.equals(track.path) : track.path == null;

    }

    @Override
    public int hashCode() {
        return path != null ? path.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Track{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", path='" + path + '\'' +
                ", lengthMillis=" + lengthMillis +
                ", channel=" + channel +
                '}';
    }
}
