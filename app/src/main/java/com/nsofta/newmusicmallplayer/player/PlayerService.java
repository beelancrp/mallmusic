package com.nsofta.newmusicmallplayer.player;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nsofta.newmusicmallplayer.R;
import com.nsofta.newmusicmallplayer.database.ChannelsDAO;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.database.TracksDAO;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.Ad;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.player.model.Day;
import com.nsofta.newmusicmallplayer.player.model.MusicBlock;
import com.nsofta.newmusicmallplayer.player.model.Track;
import com.nsofta.newmusicmallplayer.sync.LoadTracksTask;
import com.nsofta.newmusicmallplayer.sync.ReadDaysTask;
import com.nsofta.newmusicmallplayer.ui.activities.MainActivity;
import com.nsofta.newmusicmallplayer.utils.CalendarUtils;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class PlayerService extends Service {

    public static final String LOG_TAG = PlayerService.class.getName();
    public static final int NOTIFICATION_ID = 123;
    public static final int PAUSED = 0;
    public static final int PLAYING = 1;
    public static boolean running;

    private final IBinder mBinder = new LocalBinder();

    private NotificationManager mNotificationManager;
    private MediaPlayer mMediaPlayer;
    private TracksManager mTracksManager;
    private AdsManager mAdsManager;
    private PlayerListener mListener;
    private int mState;
    private Handler mCheckProgressHandler;
    private Handler mUpdateTracksHandler;
    private Handler mUpdateAdsHandler;
    private MusicBlock mMusicBlock;
    private int currentAdIndex = 0;
    private boolean shouldPlayAd = false;
    private List<Channel> mChannels = new ArrayList<>();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mTracksManager = TracksManager.getInstance();
        mAdsManager = AdsManager.getInstance();
        mCheckProgressHandler = new Handler();
        mUpdateAdsHandler = new Handler();
        mUpdateTracksHandler = new Handler();
        mMediaPlayer = new MediaPlayer();
        mNotificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(NOTIFICATION_SERVICE);
        mState = PAUSED;
        running = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = getNotificationBuilder(mTracksManager.getCurrent()).build();
        startForeground(NOTIFICATION_ID, notification);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCheckProgressHandler.removeCallbacksAndMessages(null);
        mUpdateTracksHandler.removeCallbacksAndMessages(null);
        mUpdateAdsHandler.removeCallbacksAndMessages(null);
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        mListener = null;
        mNotificationManager.cancel(NOTIFICATION_ID);
        running = false;
    }

    private Notification.Builder getNotificationBuilder(Track track) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        return new Notification.Builder(getBaseContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(getString(R.string.app_name))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(track != null ? getString(R.string.channels_current_playing, track.title) : "")
                .setContentIntent(pendingIntent);
    }

    private void postHandler() {
        mCheckProgressHandler.postDelayed(() -> {
            int millis = mMediaPlayer.getCurrentPosition();
            if (mListener != null) {
                mListener.onProgressChanged(millis);
            }
            if (mState == PLAYING) {
                postHandler();
            }
        }, 1000);
    }

    private void postUpdate(final Channel channel, final long millis) {
        Log.e(LOG_TAG, "Updating channel time: " + millis);
        final int hour = 60_000;
        mUpdateTracksHandler.postDelayed(() -> {
            Log.d(LOG_TAG, "Updating channel: " + channel.toString());
            new LoadTracksTask(PlayerService.this) {
                @Override
                protected void onPostExecute(List<Track> tracks) {
                    Log.d(LOG_TAG, "Tracks empty: " + tracks.isEmpty());
                    if (mListener != null && !tracks.isEmpty()) {
                        mListener.onPlaylistChanged(mChannels);
                    }
                    postUpdate(channel, 55 * hour);
                }
            }.execute(channel);
        }, millis);
    }

    private void checkDays() {
        new ReadDaysTask(this) {
            @Override
            protected void onPostExecute(List<Day> days) {
                int dayOfWeek = CalendarUtils.getDayOfWeek();
                Day day = days.get(dayOfWeek);
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    Date today = new Date();
                    Date startTime = dateFormat.parse(day.timeStart);
                    Date finishTime = dateFormat.parse(day.timeFinish);
                    if (today.before(startTime) || today.after(finishTime)) {
                        setVolume(0);
                        pause();
                    } else {
                        for (MusicBlock mb : day.musicBlocks) {
                            Date startMb = dateFormat.parse(mb.start);
                            Date finishMb = dateFormat.parse(mb.finish);
                            if (today.after(startMb) && today.before(finishMb) && !mMusicBlock.equals(mb)) {
                                mMusicBlock = mb;
                                new SetMusicBlockTask().execute(mb);
                            }
                        }
                    }
                } catch (ParseException e) {
                    Logger.logError(LOG_TAG, e);
                }
            }
        }.execute();
    }

    public void pause() {
        mMediaPlayer.pause();
        mState = PAUSED;
        mCheckProgressHandler.removeCallbacksAndMessages(null);
    }

    public void play() {
        mMediaPlayer.start();
        mState = PLAYING;
        postHandler();
    }

    public int getState() {
        return mState;
    }

    public int getCurrentPosition() {
        return mMediaPlayer.getCurrentPosition();
    }

    public void setPlayerListener(PlayerListener listener) {
        mListener = listener;
        startPlay();
    }

    public void startPlay() {
        if (mTracksManager.isNoTracks()) {
            Log.d(LOG_TAG, "Empty tracks. Getting tracks");
            new GetTracksTask() {
                @Override
                protected void onPostExecute(List<Track> tracks) {
                    mTracksManager.setTracks(tracks);
                    if (!tracks.isEmpty()) {
                        mState = PLAYING;
                        setTrack(mTracksManager.getCurrent());
                    }
                    if (mListener != null) mListener.onPlaylistChanged(mChannels);
                    for (Channel ch : mChannels) {
                        postUpdate(ch, 55 * 60_000);
                    }
                    updateAds();
                }
            }.execute();
        } else {
            setTrack(mTracksManager.getCurrent());
            updateAds();
            if (mListener != null) mListener.onPlaylistChanged(mChannels);
        }
    }

    public void setVolume(int percents) {
        int value = (int) (15 * ((float) percents / 100));
        AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mgr.setStreamVolume(AudioManager.STREAM_MUSIC, value, 0);
    }

    public void seekTo(int millis) {
        mMediaPlayer.seekTo(millis);
    }

    public void setTrack(final Track track) {
        if (track == null) return;
        mCheckProgressHandler.postDelayed(() -> {
            if (!track.equals(mTracksManager.getCurrent())) return;
            if (PreferenceUtils.isCorporateEdition(PlayerService.this)) {
                checkDays();
            }
            new DecryptTrackTask(PlayerService.this) {
                @Override
                protected void onPostExecute(Uri uri) {
                    if (mMediaPlayer == null) return;
                    setVolume(PreferenceUtils.getMusicVolume(PlayerService.this));
                    try {
                        mMediaPlayer.reset();
                        mMediaPlayer.setDataSource(PlayerService.this, uri);
                        mMediaPlayer.prepare();
                        if (mState == PLAYING) {
                            play();
                        }
                        if (mListener != null) mListener.onTrackChanged(track);
                        mNotificationManager.notify(NOTIFICATION_ID, getNotificationBuilder(track).build());
                    } catch (IOException e) {
                        Logger.logError(LOG_TAG, e);
                        return;
                    }
                    mMediaPlayer.setOnCompletionListener(mp -> {
                        if (mAdsManager.isHasAdsStack() && shouldPlayAd) {
                            shouldPlayAd = false;
                            Ad ad = mAdsManager.getTopStackAd();
                            new DecryptAdTask(PlayerService.this) {
                                @Override
                                protected void onPostExecute(Uri uri1) {
                                    try {
                                        pause();
                                        mMediaPlayer.reset();
                                        mMediaPlayer.setDataSource(PlayerService.this, uri1);
                                        mMediaPlayer.prepare();
                                        if (mState == PAUSED) {
                                            play();
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.execute(ad);
                        } else {
                            setTrack(mTracksManager.jumpNext());
                        }
                    });
                }
            }.execute(track);
        }, 400);
    }

    private void updateAds() {
        if (mUpdateAdsHandler == null) mUpdateAdsHandler = new Handler();
        int fiveMinute = 5 * 60_000;
        mUpdateAdsHandler.postDelayed(AdsTimerTask, fiveMinute);
    }

    protected Runnable AdsTimerTask = new Runnable() {

        @Override
        public void run() {
            if (isValidPlayTime()) {
                if (currentAdIndex == 12) currentAdIndex = 0;
                currentAdIndex++;
                Ad ad = mAdsManager.findAdByIndex(currentAdIndex);
                if (ad != null) {
                    if (isValidAdDate(ad)) {
                        new DecryptAdTask(PlayerService.this) {
                            @Override
                            protected void onPostExecute(Uri uri) {
                                startPlayAd(uri);
                            }
                        }.execute(ad);
                    }

                }
            }
        }

        private void startPlayAd(final Uri uri) {
            if (mMediaPlayer == null) return;
            try {
                if (PreferenceUtils.isPlayAdFixed(PlayerService.this)) {
                    pause();
                    mMediaPlayer.reset();
                    mMediaPlayer.setDataSource(PlayerService.this, uri);
                    mMediaPlayer.prepare();
                    if (mState == PAUSED) {
                        play();
                    }
                    updateAds();
                } else {
                    shouldPlayAd = true;
                    updateAds();
                }
            } catch (IOException e) {
                Logger.logError(LOG_TAG, e);
            }
        }

        private boolean isValidPlayTime() {
            String path = getFilesDir().getPath() + "/" + PreferenceUtils.getDaysName(PlayerService.this);
            File file = new File(path);
            if (file.exists() && file.length() > 0) {
                List<Day> days = readDays(file);
                int dayOfWeek = CalendarUtils.getDayOfWeek();
                Day day = days.get(dayOfWeek);
                Log.d(LOG_TAG, "Got day: " + day.toString());
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    Date today = new Date();
                    Date startTime = dateFormat.parse(day.timeStart);
                    Date finishTime = dateFormat.parse(day.timeFinish);
                    if (today.before(startTime) || today.after(finishTime)) {
                        Log.d(LOG_TAG, "Not in range");
                        return false;
                    } else {
                        return true;
                    }
                } catch (ParseException e) {
                    Logger.logError(LOG_TAG, e);
                    return false;
                }
            } else {
                return false;
            }
        }

        private boolean isValidAdDate(Ad ad) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.mm.yy", Locale.getDefault());
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            Date today = new Date();
            try {
                Date startDate = dateFormat.parse(ad.beginDate);
                Date endDate = dateFormat.parse(ad.endDate);
                Date startTime = timeFormat.parse(ad.beginTime);
                Date endTime = timeFormat.parse(ad.endTime);

                if (today.after(startDate) && today.before(endDate)) {
                    if (today.after(startTime) && today.before(endTime))
                        return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return false;
        }

        private List<Day> readDays(File file) {
            List<Day> days = new ArrayList<>();
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
                days.addAll((List<Day>) in.readObject());
            } catch (IOException | ClassNotFoundException e) {
                Logger.logError(LOG_TAG, e);
            }

            return days;
        }
    };

    public class SetMusicBlockTask extends AsyncTask<MusicBlock, Void, List<Track>> {

        @Override
        protected List<Track> doInBackground(MusicBlock... params) {
            MusicBlock mb = params[0];
            List<Track> tracks = new ArrayList<>();
            try {
                TracksDAO tracksDao = HelperFactory.getHelper().getTracksDao();
                ChannelsDAO channelsDAO = HelperFactory.getHelper().getChannelsDao();
                List<Channel> channels = channelsDAO.queryForAll();
                for (Channel ch : mb.channels) {
                    Channel channel = channels.get(channels.indexOf(ch));
                    tracks.addAll(tracksDao.queryBuilder()
                            .where().eq(Track.Columns.CHANNEL, channel.id)
                            .query());
                }
            } catch (SQLException e) {
                Logger.logError(LOG_TAG, e);
            }
            return tracks;
        }

        @Override
        protected void onPostExecute(List<Track> tracks) {
            mTracksManager.setTracks(tracks);
            if (mListener != null) mListener.onPlaylistChanged(mChannels);
        }
    }

    public class GetTracksTask extends AsyncTask<Void, Void, List<Track>> {

        @Override
        protected List<Track> doInBackground(Void... params) {
            if (PreferenceUtils.isCorporateEdition(PlayerService.this)) {
                return getTodayTracks();
            } else {
                return getGoodTracks();
            }
        }

        private List<Day> readDays(File file) {
            List<Day> days = new ArrayList<>();
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
                days.addAll((List<Day>) in.readObject());
            } catch (IOException | ClassNotFoundException e) {
                Logger.logError(LOG_TAG, e);
            }
            return days;
        }

        List<Track> getGoodTracks() {
            List<Track> allTracks = new ArrayList<>();
            try {
                ChannelsDAO mChannelsDao = HelperFactory.getHelper().getChannelsDao();
                TracksDAO mTracksDao = HelperFactory.getHelper().getTracksDao();
                List<Channel> goodChannels = mChannelsDao.queryForEq(Channel.Columns.CHECKED, true);
                mChannels.clear();
                mChannels.addAll(goodChannels);
                for (Channel ch : goodChannels) {
                    allTracks.addAll(mTracksDao.queryForEq(Track.Columns.CHANNEL, ch.id));
                }
            } catch (SQLException e) {
                Logger.logError(LOG_TAG, e);
                return new ArrayList<>();
            }
            return allTracks;
        }

        List<Track> getTodayTracks() {
            Log.d(LOG_TAG, "Getting today tracks");
            List<Track> allTracks = new ArrayList<>();
            String path = getFilesDir().getPath() + "/" + PreferenceUtils.getDaysName(PlayerService.this);
            File file = new File(path);
            if (file.exists() && file.length() > 0) {
                List<Day> days = readDays(file);
                int dayOfWeek = CalendarUtils.getDayOfWeek();
                Day day = days.get(dayOfWeek);
                Log.d(LOG_TAG, "Got day: " + day.toString());
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
                    Date today = new Date();
                    Date startTime = dateFormat.parse(day.timeStart);
                    Date finishTime = dateFormat.parse(day.timeFinish);
                    if (today.before(startTime) || today.after(finishTime)) {
                        Log.d(LOG_TAG, "Not in range");
                        return new ArrayList<>();
                    } else {
                        for (MusicBlock mb : day.musicBlocks) {
                            Date startMb = dateFormat.parse(mb.start);
                            Date finishMb = dateFormat.parse(mb.finish);
                            if (today.after(startMb) && today.before(finishMb) && !mMusicBlock.equals(mb)) {
                                Log.d(LOG_TAG, "Got NEW music block: " + mb.toString());
                                mMusicBlock = mb;
                                TracksDAO tracksDao = HelperFactory.getHelper().getTracksDao();
                                ChannelsDAO channelsDAO = HelperFactory.getHelper().getChannelsDao();
                                List<Channel> channels = channelsDAO.queryForAll();
                                for (Channel ch : mb.channels) {
                                    Channel channel = channels.get(channels.indexOf(ch));
                                    allTracks.addAll(tracksDao.queryBuilder()
                                            .where().eq(Track.Columns.CHANNEL, channel.id)
                                            .query());
                                }
                            }
                        }
                    }
                } catch (ParseException | SQLException e) {
                    Logger.logError(LOG_TAG, e);
                }
            } else {
                Log.d(LOG_TAG, "File not exists");
            }
            return allTracks;
        }
    }


    public class LocalBinder extends Binder {
        public PlayerService getService() {
            return PlayerService.this;
        }
    }
}
