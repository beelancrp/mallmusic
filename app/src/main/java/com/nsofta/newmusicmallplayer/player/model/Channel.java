package com.nsofta.newmusicmallplayer.player.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class Channel implements Serializable {

    @DatabaseField(generatedId = true, columnName = Columns.ID)
    public int id;
    @DatabaseField(columnName = Columns.RATIO)
    @SerializedName("coef")
    public float ratio;
    @DatabaseField(columnName = Columns.INDEX)
    public int index = 0;
    @DatabaseField(columnName = Columns.NAME, unique = true)
    @SerializedName("channel")
    public String name;
    @DatabaseField(columnName = Columns.UPDATE_FREQ)
    @SerializedName("updateFrequency")
    public int updateFrequency;
    @DatabaseField(columnName = Columns.COLOR)
    @SerializedName("color")
    public String color;
    @DatabaseField(columnName = Columns.IMAGE)
    @SerializedName("img")
    public String img;
    @DatabaseField(columnName = Columns.DESCR)
    @SerializedName("description")
    public String description;
    @DatabaseField(columnName = Columns.CHECKED)
    public boolean checked;


    public Channel() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Channel channel = (Channel) o;

        return name != null ? name.equals(channel.name) : channel.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "ratio=" + ratio +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", updateFrequency=" + updateFrequency +
                ", color='" + color + '\'' +
                ", img='" + img + '\'' +
                '}';
    }

    public static class Columns {
        public static final String ID = "_id";
        public static final String RATIO = "ratio";
        public static final String NAME = "name";
        public static final String UPDATE_FREQ = "freq";
        public static final String COLOR = "color";
        public static final String IMAGE = "img";
        public static final String INDEX = "index";
        public static final String CHECKED = "checked";
        public static final String DESCR = "description";
    }
}
