package com.nsofta.newmusicmallplayer.player;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.provider.DocumentFile;

import com.nsofta.newmusicmallplayer.player.model.Track;
import com.nsofta.newmusicmallplayer.storage.CryptoManager;
import com.nsofta.newmusicmallplayer.storage.StorageUriBuilder;

public class DecryptTrackTask extends AsyncTask<Track, Void, Uri> {

    private Context mContext;
    private CryptoManager mCryptoManager;

    public DecryptTrackTask(Context context) {
        mContext = context;
        mCryptoManager = CryptoManager.getInstance(context);
    }

    @Override
    protected Uri doInBackground(Track... params) {
        Track track = params[0];
        DocumentFile trackFile = mCryptoManager.decrypt(track.title,
                StorageUriBuilder.fromFileUri(mContext, Uri.parse(track.path)).getFile());
        return trackFile.getUri();
    }
}
