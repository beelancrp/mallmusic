package com.nsofta.newmusicmallplayer.player.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;

public class MusicBlock implements Serializable {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    @SerializedName("start")
    public String start;
    @DatabaseField
    @SerializedName("finish")
    public String finish;
    @SerializedName("chenals")
    public ArrayList<Channel> channels;

    @Override
    public String toString() {
        return "MusicBlock{" +
                "id=" + id +
                ", start='" + start + '\'' +
                ", finish='" + finish + '\'' +
                ", channels=" + channels +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MusicBlock that = (MusicBlock) o;

        if (start != null ? !start.equals(that.start) : that.start != null) return false;
        return finish != null ? finish.equals(that.finish) : that.finish == null;

    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (finish != null ? finish.hashCode() : 0);
        return result;
    }
}
