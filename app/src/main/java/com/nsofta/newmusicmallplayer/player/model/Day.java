package com.nsofta.newmusicmallplayer.player.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;

public class Day implements Serializable {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    @SerializedName("day")
    @Expose
    public String day;
    @DatabaseField
    @SerializedName("time_start")
    @Expose
    public String timeStart;
    @DatabaseField
    @SerializedName("time_finish")
    @Expose
    public String timeFinish;
    @DatabaseField
    @SerializedName("music_volume")
    @Expose
    public int musicVolume;
    @DatabaseField
    @SerializedName("adv_volume")
    @Expose
    public int advVolume;
    @SerializedName("music_blocks")
    @Expose
    public ArrayList<MusicBlock> musicBlocks;

    @Override
    public String toString() {
        return "Day{" +
                "id=" + id +
                ", day='" + day + '\'' +
                ", timeStart='" + timeStart + '\'' +
                ", timeFinish='" + timeFinish + '\'' +
                ", musicVolume=" + musicVolume +
                ", advVolume=" + advVolume +
                ", musicBlocks=" + musicBlocks +
                '}';
    }
}
