package com.nsofta.newmusicmallplayer.player.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Illya on 24.04.2017.
 */

public class BlockedTrack {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField(columnName = Columns.CHANNEL)
    @SerializedName("channel")
    @Expose
    public String chsnnelName;
    @DatabaseField(columnName = Columns.NAME)
    @SerializedName("file_name")
    @Expose
    public String fileName;
    @SerializedName("folder")
    @Expose
    public String folder;

    public static class Columns {
        public static final String NAME = "name";
        public static final String CHANNEL = "channel";
    }
}
