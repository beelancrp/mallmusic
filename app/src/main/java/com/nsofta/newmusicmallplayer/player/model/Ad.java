package com.nsofta.newmusicmallplayer.player.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ad implements Serializable {

    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField(columnName = Columns.PATH)
    public String filePath;
    @DatabaseField(columnName = Columns.AD_NAME)
    @SerializedName("adv_name")
    public String adName;
    @DatabaseField
    @SerializedName("data_begin")
    public String beginDate;
    @DatabaseField
    @SerializedName("data_end")
    public String endDate;
    @DatabaseField
    @SerializedName("time_begin")
    public String beginTime;
    @DatabaseField
    @SerializedName("time_end")
    public String endTime;
    @DatabaseField
    @SerializedName("count1")
    public boolean count1;
    @DatabaseField
    @SerializedName("count2")
    public boolean count2;
    @DatabaseField
    @SerializedName("count3")
    public boolean count3;
    @DatabaseField
    @SerializedName("count4")
    public boolean count4;
    @DatabaseField
    @SerializedName("count5")
    public boolean count5;
    @DatabaseField
    @SerializedName("count6")
    public boolean count6;
    @DatabaseField
    @SerializedName("count7")
    public boolean count7;
    @DatabaseField
    @SerializedName("count8")
    public boolean count8;
    @DatabaseField
    @SerializedName("count9")
    public boolean count9;
    @DatabaseField
    @SerializedName("count10")
    public boolean count10;
    @DatabaseField
    @SerializedName("count11")
    public boolean count11;
    @DatabaseField
    @SerializedName("count12")
    public boolean count12;
    @DatabaseField
    public String ext;
    @DatabaseField(columnName = Columns.LAST_PLAYED_INDEX, defaultValue = "true")
    public boolean lastPlayedIndex;

    public Ad() {
    }

    public static class Columns {
        public static final String AD_NAME = "ad_name";
        public static final String PATH = "path";
        public static final String LAST_PLAYED_INDEX = "last_index";
    }

    public List<Boolean> getIndexMap() {
        List<Boolean> indexes = new ArrayList<>();
        indexes.add(0, false);
        indexes.add(1, count1);
        indexes.add(2, count2);
        indexes.add(3, count3);
        indexes.add(4, count4);
        indexes.add(5, count5);
        indexes.add(6, count6);
        indexes.add(7, count7);
        indexes.add(8, count8);
        indexes.add(9, count9);
        indexes.add(10, count10);
        indexes.add(11, count11);
        indexes.add(12, count12);
        return indexes;
    }
}
