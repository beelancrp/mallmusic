package com.nsofta.newmusicmallplayer.player;

import com.nsofta.newmusicmallplayer.player.model.Track;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TracksManager {

    public static final String LOG_TAG = TracksManager.class.getName();
    private static TracksManager sInstance = new TracksManager();

    public static TracksManager getInstance() {
        return sInstance;
    }

    private TracksManager() {
    }

    private List<Track> mTracks = new LinkedList<>();
    private int mTrackIndex;

    public Track getCurrent() {
        if (mTracks == null || mTracks.isEmpty()) return null;
        return mTracks.get(mTrackIndex);
    }

    public Track jumpNext() {
        if (mTracks == null || mTracks.isEmpty()) {
            return null;
        }
        mTrackIndex++;
        mTrackIndex %= mTracks.size();
        return mTracks.get(mTrackIndex);
    }

    public Track jumpPrev() {
        if (mTracks == null || mTracks.isEmpty()) {
            return null;
        }
        mTrackIndex--;
        if (mTrackIndex < 0) mTrackIndex = 0;
        mTrackIndex %= mTracks.size();
        return mTracks.get(mTrackIndex);
    }

    public void jumpTo(Track track) {
        int index = mTracks.indexOf(track);
        if (index > -1) mTrackIndex = index;
    }

    public List<Track> getTracks() {
        return mTracks;
    }

    public boolean isNoTracks() {
        return mTracks.isEmpty();
    }

    public void eraseTracks() {
        mTracks.clear();
    }

    public boolean setTracks(List<Track> tracks) {
        mTracks.clear();
        cleverShuffle(tracks);
        mTracks.addAll(tracks);
        mTrackIndex = 0;
        return true;
    }

    public void cleverShuffle(List<Track> tracks) {
        Collections.shuffle(tracks);
    }
}
