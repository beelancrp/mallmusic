package com.nsofta.newmusicmallplayer.eventbus;

/**
 * Created by Illya on 19.03.2017.
 */

public class ChannelCheckedEvent {
    int channelId;
    boolean checked;

    public ChannelCheckedEvent(int channelId, boolean checked) {
        this.channelId = channelId;
        this.checked = checked;
    }

    public int getChannelId() {
        return channelId;
    }

    public boolean isChecked() {
        return checked;
    }
}
