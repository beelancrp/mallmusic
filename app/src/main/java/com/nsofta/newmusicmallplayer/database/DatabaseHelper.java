package com.nsofta.newmusicmallplayer.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.nsofta.newmusicmallplayer.player.model.Ad;
import com.nsofta.newmusicmallplayer.player.model.BlockedTrack;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.player.model.Track;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static final int VERSION = 1;
    public static final String NAME = "database.db";

    private ChannelsDAO mChannelsDao;
    private TracksDAO mTracksDao;
    private AdsDAO mAdsDao;
    private BlockedTrackDAO mBlockedTracksDao;

    public DatabaseHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Channel.class);
            TableUtils.createTable(connectionSource, Track.class);
            TableUtils.createTable(connectionSource, Ad.class);
            TableUtils.createTable(connectionSource, BlockedTrack.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Channel.class, true);
            TableUtils.dropTable(connectionSource, Track.class, true);
            TableUtils.dropTable(connectionSource, Ad.class, true);
            TableUtils.dropTable(connectionSource, BlockedTrack.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //выполняется при закрытии приложения
    @Override
    public void close() {
        super.close();
        mChannelsDao = null;
        mTracksDao = null;
        mAdsDao = null;
    }

    public ChannelsDAO getChannelsDao() throws SQLException {
        if (mChannelsDao == null) {
            mChannelsDao = new ChannelsDAO(getConnectionSource(), Channel.class);
        }
        return mChannelsDao;
    }

    public TracksDAO getTracksDao() throws SQLException {
        if (mTracksDao == null) {
            mTracksDao = new TracksDAO(getConnectionSource(), Track.class);
        }
        return mTracksDao;
    }

    public AdsDAO getAdsDao() throws SQLException {
        if (mAdsDao == null) {
            mAdsDao = new AdsDAO(getConnectionSource(), Ad.class);
        }
        return mAdsDao;
    }

    public BlockedTrackDAO getBlockedTracksDao() throws SQLException {
        if (mBlockedTracksDao == null) {
            mBlockedTracksDao = new BlockedTrackDAO(getConnectionSource(), BlockedTrack.class);
        }
        return mBlockedTracksDao;
    }
}
