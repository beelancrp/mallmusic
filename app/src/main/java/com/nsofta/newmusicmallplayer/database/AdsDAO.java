package com.nsofta.newmusicmallplayer.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.nsofta.newmusicmallplayer.player.model.Ad;

import java.sql.SQLException;

public class AdsDAO extends BaseDaoImpl<Ad, Integer> {

    protected AdsDAO(ConnectionSource connectionSource, Class<Ad> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

}