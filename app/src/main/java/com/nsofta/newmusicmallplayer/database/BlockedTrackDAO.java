package com.nsofta.newmusicmallplayer.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.nsofta.newmusicmallplayer.player.model.BlockedTrack;

import java.sql.SQLException;

/**
 * Created by Illya on 24.04.2017.
 */

public class BlockedTrackDAO extends BaseDaoImpl<BlockedTrack, Integer> {

    protected BlockedTrackDAO(ConnectionSource connectionSource, Class<BlockedTrack> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

}
