package com.nsofta.newmusicmallplayer.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.nsofta.newmusicmallplayer.player.model.Track;

import java.sql.SQLException;

public class TracksDAO extends BaseDaoImpl<Track, Integer> {
    protected TracksDAO(ConnectionSource connectionSource, Class<Track> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
