package com.nsofta.newmusicmallplayer.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.nsofta.newmusicmallplayer.player.model.Channel;

import java.sql.SQLException;

public class ChannelsDAO extends BaseDaoImpl<Channel, Integer> {
    protected ChannelsDAO(ConnectionSource connectionSource, Class<Channel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }
}
