package com.nsofta.newmusicmallplayer.ftp;

import android.os.Parcel;
import android.os.Parcelable;

public class FTPDirectory implements Parcelable {

    private StringBuilder builder;

    public FTPDirectory(String host) {
        builder = new StringBuilder(host);
    }

    protected FTPDirectory(Parcel in) {
        builder = new StringBuilder(in.readString());
    }

    public static final Creator<FTPDirectory> CREATOR = new Creator<FTPDirectory>() {
        @Override
        public FTPDirectory createFromParcel(Parcel in) {
            return new FTPDirectory(in);
        }

        @Override
        public FTPDirectory[] newArray(int size) {
            return new FTPDirectory[size];
        }
    };

    public FTPDirectory append(String path) {
        builder.append("/").append(path);
        return this;
    }

    public String getPath() {
        return builder.toString();
    }

    public static FTPDirectory getImgDirectory() {
        return new FTPDirectory("/img");
    }

    public static FTPDirectory getAdDirectory() {
        return new FTPDirectory("/adv");
    }

    public static FTPDirectory getMusicDirectory() {
        return new FTPDirectory("/music");
    }

    public static FTPDirectory getChannelDirectory(String channel, int index) {
        return getChannelRootDirectory(channel)
                .append(Integer.toString(index));
    }

    public static FTPDirectory getChannelRootDirectory(String channel) {
        return getMusicDirectory()
                .append(channel);
    }

    @Override
    public String toString() {
        return "FTPDirectory{" +
                "Path=" + builder.toString() +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getPath());
    }
}
