package com.nsofta.newmusicmallplayer.ftp;

import android.util.Log;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;

public class FTPManager {

    public static final String LOG_TAG = FTPManager.class.getName();
    private static final String HOST = "musicmall.com.ua";
    private static final String LOGIN = "android_test";
    private static final String PASSWORD = "android_test";
    private static final int REPEAT_TIMEOUT_MILLIS = 5 * 1000;

    private int mCommandAttempts;

    private String host;
    private String login;
    private String password;
    private FTPClient mFtpClient;

    private FTPManager(String host, String login, String password) {
        this.host = host;
        this.login = login;
        this.password = password;
        mFtpClient = new FTPClient();
    }

    public FTPManager() {
        this(HOST, LOGIN, PASSWORD);
    }

    public boolean connect() {
        mCommandAttempts = 5;
        return attemptConnect();
    }

    private boolean attemptConnect() {
        try {
            mFtpClient.connect(host);
            mFtpClient.login(this.login, password);
            mFtpClient.setPassive(true);
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
            try {
                TimeUnit.MILLISECONDS.sleep(REPEAT_TIMEOUT_MILLIS);
            } catch (InterruptedException e1) {
                Log.e(LOG_TAG, e.toString());
                return false;
            }
            mCommandAttempts--;
            if (mCommandAttempts > 0) {
                return attemptConnect();
            }
        }
        return true;
    }

    public List<String> listFiles() {
        return listFiles(FTPFile.TYPE_FILE);
    }

    public List<String> listDirectores() {
        return listFiles(FTPFile.TYPE_DIRECTORY);
    }

    public List<String> listFiles(int type) {
        mCommandAttempts = 5;
        return attemptListFiles(type);
    }

    private List<String> attemptListFiles(int type) {
        ArrayList<String> names = new ArrayList<>();
        try {
            FTPFile[] files = mFtpClient.list();
            for (FTPFile file : files) {
                if (file.getType() == type) {
                    names.add(file.getName());
                }
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
            try {
                TimeUnit.MILLISECONDS.sleep(REPEAT_TIMEOUT_MILLIS);
            } catch (InterruptedException e1) {
                Log.e(LOG_TAG, e.toString());
                return names;
            }
            mCommandAttempts--;
            if (mCommandAttempts > 0) {
                return attemptListFiles(type);
            }
        }
        return names;
    }

    public boolean downloadFile(OutputStream outputStream, String ftpName) {
        mCommandAttempts = 5;
        return attemptDownloadFile(outputStream, ftpName);
    }

    private boolean attemptDownloadFile(OutputStream outputStream, String ftpName) {
        try {
            mFtpClient.download(ftpName, outputStream, 0, null);
            return true;
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
            try {
                TimeUnit.MILLISECONDS.sleep(REPEAT_TIMEOUT_MILLIS);
            } catch (InterruptedException e1) {
                Log.e(LOG_TAG, e.toString());
                return false;
            }
            mCommandAttempts--;
            if (mCommandAttempts > 0) {
                return attemptDownloadFile(outputStream, ftpName);
            }
        }
        return false;
    }

    public boolean changeDirectory(String path) {
        mCommandAttempts = 5;
        return attemptChangeDirectory(path);
    }

    private boolean attemptChangeDirectory(String path) {
        try {
            mFtpClient.changeDirectory(path);
            return true;
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
            try {
                TimeUnit.MILLISECONDS.sleep(REPEAT_TIMEOUT_MILLIS);
            } catch (InterruptedException e1) {
                Log.e(LOG_TAG, e.toString());
                return false;
            }
            mCommandAttempts--;
            if (mCommandAttempts > 0) {
                return attemptChangeDirectory(path);
            }
        }
        return false;
    }

    public boolean release() {
        if (mFtpClient != null) try {
            mFtpClient.logout();
            mFtpClient.disconnect(true);
            mFtpClient = null;
            return true;
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
            return false;
        }
        return false;
    }

    public boolean isConnected() {
        return mFtpClient.isConnected();
    }
}
