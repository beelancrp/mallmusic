package com.nsofta.newmusicmallplayer.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.nsofta.newmusicmallplayer.database.ChannelsDAO;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.Channel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChannelsLoader extends AsyncTaskLoader<List<Channel>> {

    public static final String LOG_TAG = ChannelsLoader.class.getName();

    public ChannelsLoader(Context context) {
        super(context);
    }

    @Override
    public List<Channel> loadInBackground() {
        List<Channel> channels = new ArrayList<>();
        try {
            ChannelsDAO channelsDao = HelperFactory.getHelper().getChannelsDao();
            channels.addAll(channelsDao.queryForAll());
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);
        }
        return channels;
    }
}
