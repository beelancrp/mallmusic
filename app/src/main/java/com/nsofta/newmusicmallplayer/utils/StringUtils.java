package com.nsofta.newmusicmallplayer.utils;

import java.util.Random;

public class StringUtils {

    private static Random sRandom = new Random();

    public static String generateString() {
        return generateString(sRandom,
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 20);
    }

    static String generateString(Random rng, String characters, int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }

}
