package com.nsofta.newmusicmallplayer.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.nsofta.newmusicmallplayer.api.model.AdResponse;

public class PreferenceUtils {

    public static void setId(Context context, int version) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putInt("id", version)
                .apply();
    }

    public static int getId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt("id", -1);
    }

    public static void setName(Context context, String name) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("name", name)
                .apply();
    }

    public static String getName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString("name", "");
    }

    public static void setSdPath(Context context, String path) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("path", path)
                .apply();
    }

    public static String getSdPath(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString("path", "");
    }

    public static void setFTPAddress(Context context, String address) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("ftp_address", address)
                .apply();
    }

    public static String getFTPAddress(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString("ftp_address", "");
    }

    public static void setFTPLogin(Context context, String login) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("ftp_login", login)
                .apply();
    }

    public static String getFTPLogin(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString("ftp_login", "");
    }

    public static void setFTPPassword(Context context, String password) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("ftp_password", password)
                .apply();
    }

    public static String getFTPPassword(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString("ftp_password", "");
    }

    public static void setCorporateEdition(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean("corporate", value)
                .apply();
    }

    public static boolean isCorporateEdition(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean("corporate", false);
    }

    public static void setMusicVolume(Context context, int value) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putInt("music_volume", value)
                .apply();
    }

    public static int getMusicVolume(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt("music_volume", 40);
    }

    public static void setAdvVolume(Context context, int value) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putInt("adv_volume", value)
                .apply();
    }

    public static int getAdvVolume(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt("adv_volume", 100);
    }

    public static void setPushId(Context context, String pushId) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("push", pushId)
                .apply();
    }

    public static String getPushId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString("push", "");
    }

    public static void setHasAds(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean("has_ads", value)
                .apply();
    }

    public static boolean hasAds(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean("has_ads", false);
    }

    public static void setPlayAdFixed(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean("fixed_time", value)
                .apply();
    }

    public static boolean isPlayAdFixed(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean("fixed_time", false);
    }

    public static void setDaysName(Context context, String name) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("days", name)
                .apply();
    }

    public static String getDaysName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString("days", "days");
    }

    public static void setAdResponse(Context context, AdResponse response) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString("ads", new Gson().toJson(response))
                .apply();
    }

    public static AdResponse getAdResponse(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return new Gson().fromJson(preferences.getString("ads", null), AdResponse.class);
    }

}
