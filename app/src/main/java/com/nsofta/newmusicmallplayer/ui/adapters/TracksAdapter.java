package com.nsofta.newmusicmallplayer.ui.adapters;

import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nsofta.newmusicmallplayer.R;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.storage.StorageUriBuilder;
import com.nsofta.newmusicmallplayer.ui.VerticalSeekBar;

import java.util.List;

public class TracksAdapter extends RecyclerView.Adapter<TracksAdapter.TrackViewHolder> {

    private List<Channel> mChannels;

    public TracksAdapter() {
    }

    public TracksAdapter(List<Channel> channels) {
        mChannels = channels;
    }

    @Override
    public TrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TrackViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_track, parent, false));
    }

    @Override
    public void onBindViewHolder(TrackViewHolder holder, int position) {
        final Channel item = mChannels.get(position);

        if (!TextUtils.isEmpty(item.color))
            holder.coverImageView.setCardBackgroundColor(Color.parseColor(item.color));

        holder.titleTextView.setText(item.name);
    }

    @Override
    public int getItemCount() {
        return mChannels == null ? 0 : mChannels.size();
    }

    public void setChannels(List<Channel> channels) {
        mChannels = channels;
        this.notifyDataSetChanged();
    }

    static class TrackViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView;
        CardView coverImageView;
        VerticalSeekBar seekBar;

        TrackViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.txt_title);
            coverImageView = (CardView) itemView.findViewById(R.id.color);
            seekBar = (VerticalSeekBar) itemView.findViewById(R.id.seekBar);
            seekBar.setThumb(ContextCompat.getDrawable(itemView.getContext(), R.drawable.ic_dot));
        }
    }


}
