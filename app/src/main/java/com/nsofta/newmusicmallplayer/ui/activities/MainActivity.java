package com.nsofta.newmusicmallplayer.ui.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nsofta.newmusicmallplayer.R;
import com.nsofta.newmusicmallplayer.api.MallRestService;
import com.nsofta.newmusicmallplayer.api.model.MusicResponse;
import com.nsofta.newmusicmallplayer.database.ChannelsDAO;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.loaders.ChannelsLoader;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.PlayerService;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.ui.fragments.MallFragment;
import com.nsofta.newmusicmallplayer.ui.fragments.PlayerFragment;
import com.nsofta.newmusicmallplayer.utils.PlatformHelper;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Channel>> {

    public static final String LOG_TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (PreferenceUtils.getId(this) > -1
                && !TextUtils.isEmpty(PreferenceUtils.getName(this))
                && !TextUtils.isEmpty(PreferenceUtils.getSdPath(this))) {
            getSupportLoaderManager().initLoader(0, null, MainActivity.this).forceLoad();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new MallFragment())
                    .commit();
        }
    }

    public void setToolbar(Toolbar toolbar) {
        this.setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            new MaterialDialog.Builder(this)
                    .content(R.string.dialog_exit)
                    .autoDismiss(true)
                    .positiveText(R.string.yes)
                    .positiveColorRes(android.R.color.black)
                    .onPositive((dialog, which) -> {
                        stopService(new Intent(MainActivity.this, PlayerService.class));
                        finish();
                    })
                    .negativeText(R.string.no)
                    .negativeColorRes(android.R.color.black)
                    .show();
        }
    }

    @Override
    public Loader<List<Channel>> onCreateLoader(int id, Bundle args) {
        return new ChannelsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<Channel>> loader, List<Channel> channels) {
        if (!channels.isEmpty()) {
            runOnUiThread(() -> getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new PlayerFragment())
                    .commitAllowingStateLoss());
        } else {
            if (PlatformHelper.hasInternet(this)) {
                int id = PreferenceUtils.getId(this);
                MallRestService.getApi().getMusic(id).enqueue(new Callback<MusicResponse>() {
                    @Override
                    public void onResponse(Call<MusicResponse> call, Response<MusicResponse> response) {
                        if (response.isSuccessful() && response.body().invalidate()) {
                            ArrayList<Channel> channels = (ArrayList<Channel>) response.body().channels;
                            new SaveChannelsTask().execute(channels.toArray(new Channel[channels.size()]));
                        }
                    }

                    @Override
                    public void onFailure(Call<MusicResponse> call, Throwable t) {
                        Logger.logError(LOG_TAG, t);
                    }
                });
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Channel>> loader) {

    }

    /**
     * Сохраняет загруженные каналы
     */
    private class SaveChannelsTask extends AsyncTask<Channel, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Channel... channels) {
            try {
                ChannelsDAO channelsDao = HelperFactory.getHelper().getChannelsDao();
                for (Channel ch : channels) {
                    channelsDao.createOrUpdate(ch);
                }
                return !channelsDao.queryForAll().isEmpty();
            } catch (SQLException e) {
                Logger.logError(LOG_TAG, e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new PlayerFragment())
                    .commitAllowingStateLoss();
        }
    }
}
