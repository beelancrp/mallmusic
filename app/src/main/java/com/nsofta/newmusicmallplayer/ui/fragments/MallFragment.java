package com.nsofta.newmusicmallplayer.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.provider.DocumentFile;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.nsofta.newmusicmallplayer.R;
import com.nsofta.newmusicmallplayer.api.MallRestService;
import com.nsofta.newmusicmallplayer.api.model.AdResponse;
import com.nsofta.newmusicmallplayer.api.model.BaseResponse;
import com.nsofta.newmusicmallplayer.api.model.MusicResponse;
import com.nsofta.newmusicmallplayer.api.model.ObjectResponse;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.AdsManager;
import com.nsofta.newmusicmallplayer.player.model.Ad;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.player.model.Day;
import com.nsofta.newmusicmallplayer.player.model.MallInfo;
import com.nsofta.newmusicmallplayer.player.model.Track;
import com.nsofta.newmusicmallplayer.sync.LoadAdsTask;
import com.nsofta.newmusicmallplayer.sync.LoadPicturesTask;
import com.nsofta.newmusicmallplayer.sync.LoadTracksTask;
import com.nsofta.newmusicmallplayer.sync.SaveDaysTask;
import com.nsofta.newmusicmallplayer.utils.PlatformHelper;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MallFragment extends Fragment {

    public static final String LOG_TAG = MallFragment.class.getName();
    public static final int FILE_CODE = 123;

    @BindView(R.id.edit_number)
    protected EditText numberEditText;
    @BindView(R.id.wrapper_number)
    protected TextInputLayout numberInputLayout;
    @BindView(R.id.edit_sd_path)
    protected EditText pathEditText;
    @BindView(R.id.wrapper_sd_path)
    protected TextInputLayout pathInputLayout;
    @BindView(R.id.btn_save)
    protected Button saveButton;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    protected MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle(R.string.fragment_edit_info);
        int id = PreferenceUtils.getId(view.getContext());
        numberEditText.setText(id > -1 ? Integer.toString(id) : "");
        pathEditText.setText(PreferenceUtils.getSdPath(view.getContext()));
        if (id != -1) {
            updateUI();
        }
    }

    @OnClick(R.id.edit_sd_path)
    protected void onPathClick() {
        startFilePicker();
    }

    @OnClick(R.id.btn_save)
    protected void onSaveClick() {
        if (invalidate(numberEditText, numberInputLayout)
                & invalidate(pathEditText, pathInputLayout)) {
            final int id = Integer.parseInt(numberEditText.getText().toString());
            PreferenceUtils.setId(getActivity(), id);
            PreferenceUtils.setSdPath(getActivity(), pathEditText.getText().toString());

            if (PlatformHelper.hasInternet(getActivity())) {
                showLoadDialog();
                MallRestService.getApi().getObject(id).enqueue(new Callback<ObjectResponse>() {
                    @Override
                    public void onResponse(Call<ObjectResponse> call, Response<ObjectResponse> response) {
                        if (!isAdded()) return;
                        if (response.isSuccessful() && response.body().invalidate()) {
                            MallInfo mallInfo = response.body().mallInfo;
                            PreferenceUtils.setName(getActivity(), mallInfo.name);
                            PreferenceUtils.setFTPAddress(getActivity(), mallInfo.ftpHost);
                            PreferenceUtils.setFTPLogin(getActivity(), mallInfo.ftpLogin);
                            PreferenceUtils.setFTPPassword(getActivity(), mallInfo.ftpPassword);
                            boolean corporateEdition = mallInfo.corporateEdition;
                            PreferenceUtils.setCorporateEdition(getActivity(), corporateEdition);
                            PreferenceUtils.setHasAds(getActivity(), mallInfo.useAds);
                            PreferenceUtils.setPlayAdFixed(getActivity(), mallInfo.advFixedTime);
                            try {
                                HelperFactory.getHelper().getBlockedTracksDao().create(mallInfo.blockedTracks);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            String fcmToken = PreferenceUtils.getPushId(getContext());
                            MallRestService.getApi().sendToken(id, fcmToken).enqueue(new Callback<BaseResponse>() {
                                @Override
                                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                    Log.e(LOG_TAG, response.message());
                                }

                                @Override
                                public void onFailure(Call<BaseResponse> call, Throwable t) {
                                    Logger.logError(LOG_TAG, t);
                                }
                            });

                            MallRestService.getApi().getAds(id).enqueue(new Callback<AdResponse>() {
                                @Override
                                public void onResponse(Call<AdResponse> call, Response<AdResponse> response) {
                                    try {
                                        HelperFactory.getHelper().getAdsDao().create(response.body().ads);
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<AdResponse> call, Throwable t) {
                                    Logger.logError(LOG_TAG, t);
                                }
                            });

                            MallRestService.getApi().getMusic(id).enqueue(new Callback<MusicResponse>() {
                                @Override
                                public void onResponse(Call<MusicResponse> call, Response<MusicResponse> response) {
                                    try {
                                        HelperFactory.getHelper()
                                                .getChannelsDao().create(response.body().channels);
                                        new LoadPicturesTask(getActivity()).execute();
                                        List<Channel> channels = response.body().channels;
                                        new LoadTracksTask(getActivity()) {
                                            Channel prevChannel = new Channel();

                                            @Override
                                            protected void onPreExecute() {
                                                dialog.setMaxProgress(channels.size());
                                            }

                                            @Override
                                            protected void onProgressUpdate(Channel... values) {
                                                if (!values[0].equals(prevChannel)) {
                                                    prevChannel = values[0];
                                                    updateDialogProgress();
                                                }
                                            }

                                            @Override
                                            protected void onPostExecute(List<Track> tracks) {
                                                new LoadAdsTask(getActivity()) {
                                                    Ad prevAd = new Ad();

                                                    @Override
                                                    protected void onPreExecute() {
                                                        try {
                                                            List<Ad> ads = HelperFactory.getHelper().getAdsDao().queryForAll();
                                                            if (ads != null && !ads.isEmpty()) {
                                                                dialog.setProgress(0);
                                                                dialog.setMaxProgress(ads.size());
                                                            } else {
                                                                this.onPostExecute(null);
                                                            }
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                        }
                                                        dialog.setContent(R.string.ad_loading_dialog_text);
                                                    }

                                                    @Override
                                                    protected void onProgressUpdate(Ad... values) {
                                                        if (!values[0].equals(prevAd)) {
                                                            prevAd = values[0];
                                                            updateDialogProgress();
                                                        }
                                                    }

                                                    @Override
                                                    protected void onPostExecute(List<Ad> ads) {
                                                        if (ads != null)
                                                            AdsManager.getInstance().setAdsList(ads);
                                                        if (corporateEdition) {
                                                            PreferenceUtils.setDaysName(getActivity(), "days");
                                                            List<Day> days = mallInfo.days;
                                                            new SaveDaysTask(getActivity()) {
                                                                @Override
                                                                protected void onPostExecute(Boolean aBoolean) {
                                                                    if (aBoolean && isAdded()) {
                                                                        hideLoadDialog();
                                                                        getFragmentManager().beginTransaction()
                                                                                .replace(R.id.container, new PlayerFragment())
                                                                                .commit();
                                                                    } else {
                                                                        Log.e(LOG_TAG, "SAVE DAYS FAILED!");
                                                                    }
                                                                }
                                                            }.execute(days.toArray(new Day[days.size()]));
                                                        } else {
                                                            if (isAdded()) {
                                                                hideLoadDialog();
                                                                getFragmentManager().beginTransaction()
                                                                        .replace(R.id.container, new PlayerFragment())
                                                                        .commit();
                                                            }
                                                        }
                                                    }
                                                }.execute();
                                            }
                                        }.execute(channels.toArray(new Channel[channels.size()]));
                                    } catch (SQLException e) {
                                        Logger.logError(LOG_TAG, e);
                                    }
                                }

                                @Override
                                public void onFailure(Call<MusicResponse> call, Throwable t) {
                                    Logger.logError(LOG_TAG, t);
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(Call<ObjectResponse> call, Throwable t) {
                        Logger.logError(LOG_TAG, t);
                    }
                });
            } else {
                Toast.makeText(getActivity(), R.string.error_no_connection, Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getActivity(), R.string.error_fill_fields, Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_CODE && resultCode == Activity.RESULT_OK) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                Uri uri = data.getData();
                pathEditText.setText(uri.toString());
            } else {
                Uri uri = DocumentFile.fromFile(new File(data.getData().getPath())).getUri();
                pathEditText.setText(uri.toString());
            }
        }
    }

    private void showLoadDialog() {
        dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_get_object_title)
                .content(R.string.dialog_get_object_content)
                .progress(false, 0)
                .cancelable(false)
                .build();
        dialog.show();
    }

    private void updateDialogProgress() {
        dialog.incrementProgress(1);
    }

    private void hideLoadDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    protected void updateUI() {
        Context context = getActivity();
        boolean corporate = PreferenceUtils.isCorporateEdition(context);
        if (!corporate) {
            numberEditText.setEnabled(false);
            numberEditText.setText(String.valueOf(PreferenceUtils.getId(context)));
        }
    }

    protected void startFilePicker() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            startActivityForResult(intent, FILE_CODE);
        } else {
            Intent i = new Intent(getActivity(), FilePickerActivity.class);
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
            i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_DIR);
            i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());
            startActivityForResult(i, FILE_CODE);
        }
    }

    protected boolean invalidate(EditText editText, TextInputLayout parent) {
        if (TextUtils.isEmpty(editText.getText())) {
            parent.setError(getString(R.string.error_empty));
            return false;
        } else {
            parent.setErrorEnabled(false);
            return true;
        }
    }
}
