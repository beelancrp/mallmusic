package com.nsofta.newmusicmallplayer.ui.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.nsofta.newmusicmallplayer.R;
import com.nsofta.newmusicmallplayer.database.ChannelsDAO;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.eventbus.ChannelCheckedEvent;
import com.nsofta.newmusicmallplayer.eventbus.UpdateEvent;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.ui.activities.MainActivity;
import com.nsofta.newmusicmallplayer.ui.adapters.ChannelsAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChannelsFragment extends Fragment {

    public static final String LOG_TAG = ChannelsFragment.class.getName();

    @BindView(R.id.list_channels)
    protected RecyclerView channelsRecyclerView;
    @BindView(R.id.swipe_refresh)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    private ChannelsAdapter mAdapter;
    private List<Channel> mChannels = new ArrayList<>();

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_channels, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity) getActivity()).setToolbar(toolbar);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle(R.string.fragment_channels);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        channelsRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(channelsRecyclerView.getContext(),
                layoutManager.getOrientation());
        channelsRecyclerView.addItemDecoration(dividerItemDecoration);
        channelsRecyclerView.setHasFixedSize(true);
        mAdapter = new ChannelsAdapter(getContext());
        channelsRecyclerView.setAdapter(mAdapter);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(false);
            loadChannels();
        });
        loadChannels();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_channels, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                new SaveCheckedChannelTask().execute();
                break;
        }
        return false;
    }

    private void loadChannels() {
        try {
            ChannelsDAO channelsDao = HelperFactory.getHelper().getChannelsDao();
            mChannels = channelsDao.queryForAll();
            mAdapter.setChannels(mChannels);
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCheckedEvent(ChannelCheckedEvent event) {
        if (event != null)
            for (int i = 0; i < mChannels.size(); i++) {
                if (mChannels.get(i).id == event.getChannelId())
                    mChannels.get(i).checked = event.isChecked();
            }
    }

    private class SaveCheckedChannelTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                ChannelsDAO channelsDao = HelperFactory.getHelper().getChannelsDao();
                List<Channel> channels = channelsDao.queryForAll();
                for (Channel ch : mChannels) {
                    if (channels.contains(ch)) {
                        channelsDao.update(ch);
                    }
                }
            } catch (SQLException e) {
                Logger.logError(LOG_TAG, e);
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                EventBus.getDefault().post(new UpdateEvent());
                getActivity().onBackPressed();
            }
        }
    }
}
