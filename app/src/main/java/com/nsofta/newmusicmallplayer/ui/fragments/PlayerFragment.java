package com.nsofta.newmusicmallplayer.ui.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nsofta.newmusicmallplayer.R;
import com.nsofta.newmusicmallplayer.api.MallRestService;
import com.nsofta.newmusicmallplayer.api.model.BaseResponse;
import com.nsofta.newmusicmallplayer.database.ChannelsDAO;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.eventbus.UpdateEvent;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.PlayerListener;
import com.nsofta.newmusicmallplayer.player.PlayerService;
import com.nsofta.newmusicmallplayer.player.TracksManager;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.player.model.Day;
import com.nsofta.newmusicmallplayer.player.model.MusicBlock;
import com.nsofta.newmusicmallplayer.player.model.Track;
import com.nsofta.newmusicmallplayer.ui.adapters.TracksAdapter;
import com.nsofta.newmusicmallplayer.utils.CalendarUtils;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayerFragment extends Fragment implements PlayerListener {

    public static final String LOG_TAG = PlayerFragment.class.getName();

    @BindView(R.id.btnLeft)
    protected ImageButton prevButton;
    @BindView(R.id.btnRight)
    protected ImageButton nextButton;
    @BindView(R.id.btnPlay)
    protected ImageButton playButton;
    @BindView(R.id.btnDislike)
    protected ImageButton disLikeButton;
    @BindView(R.id.chnlBtn)
    protected Button channelButton;
    @BindView(R.id.objName)
    protected TextView objectName;
    @BindView(R.id.version)
    protected TextView versionName;
    @BindView(R.id.currentTime)
    protected TextView currentTime;
    @BindView(R.id.totalTime)
    protected TextView totalTime;
    @BindView(R.id.trackName)
    protected TextView trackName;
    @BindView(R.id.seek_progress)
    protected SeekBar progressSeekBar;
    @BindView(R.id.list_songs)
    protected RecyclerView songsRecyclerView;
    @BindView(R.id.progress)
    protected ProgressBar progressBar;

    private boolean mBound;
    private TracksAdapter mAdapter;
    private PlayerService mPlayer;
    private TracksManager mTracksManager;
    private List<Channel> mChannels = new ArrayList<>();

    protected ServiceConnection mPlayerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            PlayerService.LocalBinder localBinder = (PlayerService.LocalBinder) binder;
            mPlayer = localBinder.getService();
            mPlayer.setPlayerListener(PlayerFragment.this);
            if (mPlayer.getState() == PlayerService.PAUSED)
                updateFromChannels(null);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mPlayer = null;
            mBound = false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTracksManager = TracksManager.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    mPlayer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (PreferenceUtils.isCorporateEdition(getContext()))
            versionName.setText(R.string.version_corporate);
        else
            versionName.setText(R.string.version_music);

        objectName.setText(PreferenceUtils.getName(getContext()));

        channelButton.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .add(R.id.container, new ChannelsFragment())
                    .commit();
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false);
        songsRecyclerView.setLayoutManager(layoutManager);
        songsRecyclerView.setHasFixedSize(true);
        mAdapter = new TracksAdapter();
        songsRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Intent intent = new Intent(getActivity(), PlayerService.class);
        getActivity().bindService(intent, mPlayerConnection, Context.BIND_AUTO_CREATE);
        getActivity().startService(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        if (mBound) {
            getActivity().unbindService(mPlayerConnection);
            mPlayer = null;
            mBound = false;
        }
    }

    @Subscribe
    void updateFromChannels(UpdateEvent event) {
        if (!PreferenceUtils.isCorporateEdition(getActivity()))
            new UpdateChannelsTask().execute();
        else
            new UpdateMusicBlockTask().execute();
    }

    @OnClick(R.id.btnRight)
    public void onNextClick() {
        Track next = mTracksManager.jumpNext();
        mPlayer.setTrack(next);
        updateUI();
    }

    @OnClick(R.id.btnLeft)
    public void onPrevClick() {
        Track prev = mTracksManager.jumpPrev();
        mPlayer.setTrack(prev);
        updateUI();
    }

    @OnClick(R.id.btnPlay)
    public void onPlayClick() {
        switch (mPlayer.getState()) {
            case PlayerService.PAUSED:
                if (mTracksManager.getTracks().isEmpty()) {
                    Toast.makeText(getActivity(),
                            R.string.error_no_tracks,
                            Toast.LENGTH_SHORT).show();
                } else {
                    mPlayer.play();
                    playButton.setImageResource(R.drawable.ic_pause_24dp);
                }
                break;
            case PlayerService.PLAYING:
                mPlayer.pause();
                playButton.setImageResource(R.drawable.ic_play_arrow_24dp);
                break;
        }
    }

    @OnClick(R.id.btnDislike)
    public void dislike() {
        Track currentTrack = mTracksManager.getCurrent();
        if (currentTrack == null) return;
        JSONObject blockObject = new JSONObject();
        JSONArray blockArray = new JSONArray();
        try {
            blockObject.put("channel", currentTrack.channel.name)
                    .put("file_name", currentTrack.title.replace("\'", ""))
                    .put("folder", 1);
            blockArray.put(blockObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int id = PreferenceUtils.getId(getContext());
        MallRestService.getApi().blockTrack(id, blockArray)
                .enqueue(new Callback<BaseResponse>() {
                             @Override
                             public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                                 onNextClick();
                             }

                             @Override
                             public void onFailure(Call<BaseResponse> call, Throwable t) {

                             }
                         }
                );
    }

    @Override
    public void onProgressChanged(int millis) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss", Locale.getDefault());
        Date time = new Date(millis);
        currentTime.setText(simpleDateFormat.format(time));
        progressSeekBar.setProgress(millis);
    }

    @Override
    public void onTrackChanged(Track track) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss", Locale.getDefault());
        Date time = new Date(track.lengthMillis);
        totalTime.setText(simpleDateFormat.format(time));
        updateUI();
    }

    @Override
    public void onPlaylistChanged(List<Channel> channels) {
        if (isAdded()) {
            progressBar.setVisibility(View.INVISIBLE);
            mChannels.clear();
            mChannels.addAll(channels);
            mAdapter.setChannels(mChannels);
            updateUI();
        }
    }

    public void updateUI() {
        if (isAdded()) getActivity().runOnUiThread(() -> {
            Track track = mTracksManager.getCurrent();
            if (track != null) {
                progressSeekBar.setMax(track.lengthMillis);
                trackName.setText(track.title.replace("_", " ").replace(".mp3", ""));
                if (mPlayer != null) {
                    progressSeekBar.setProgress(mPlayer.getCurrentPosition());
                    if (mPlayer.getState() == PlayerService.PLAYING) {
                        playButton.setImageResource(R.drawable.ic_pause_24dp);
                    }
                    if (mPlayer.getState() == PlayerService.PAUSED) {
                        playButton.setImageResource(R.drawable.ic_play_arrow_24dp);
                    }
                }
            }
        });
    }

    private class UpdateChannelsTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            List<Channel> channels = new ArrayList<>();
            try {
                channels.addAll(HelperFactory.getHelper().getChannelsDao().queryForEq(Channel.Columns.CHECKED, true));
                mChannels.clear();
                mChannels.addAll(channels);
            } catch (SQLException e) {
                Logger.logError(LOG_TAG, e);
                return false;
            }
            return !mChannels.isEmpty();
        }

        @Override
        protected void onPostExecute(Boolean isDone) {
            if (isDone) {
                progressBar.setVisibility(View.GONE);
                mAdapter.setChannels(mChannels);
                mTracksManager.eraseTracks();
                mPlayer.startPlay();
                updateUI();
            }
        }
    }

    private class UpdateMusicBlockTask extends AsyncTask<Void, Void, List<Channel>> {

        @Override
        protected List<Channel> doInBackground(Void... params) {
            return getTodayTracks();
        }

        @Override
        protected void onPostExecute(List<Channel> isDone) {
            if (!isDone.isEmpty()) {
                progressBar.setVisibility(View.GONE);
                mAdapter.setChannels(mChannels);
                mTracksManager.eraseTracks();
                mPlayer.startPlay();
                updateUI();
            }
        }

        List<Channel> getTodayTracks() {
            Log.d(LOG_TAG, "Getting today tracks");
            List<Channel> allTracks = new ArrayList<>();
            String path = getContext().getFilesDir().getPath() + "/" + PreferenceUtils.getDaysName(getContext());
            File file = new File(path);
            if (file.exists() && file.length() > 0) {
                try {
                    List<Day> days = readDays(file);
                    int dayOfWeek = CalendarUtils.getDayOfWeek();
                    Day day = days.get(dayOfWeek);
                    MusicBlock mb = day.musicBlocks.get(0);
                    ChannelsDAO channelsDAO = HelperFactory.getHelper().getChannelsDao();
                    List<Channel> channels = channelsDAO.queryForAll();
                    for (Channel ch : mb.channels) {
                        for (Channel ch2 : channels) {
                            if (ch.name.equals(ch2.name)) {
                                ch.checked = true;
                                channelsDAO.update(ch);
                                allTracks.add(ch);
                            }
                        }
                    }

                } catch (SQLException e) {
                    Logger.logError(LOG_TAG, e);
                }
            }
            return allTracks;
        }

        private List<Day> readDays(File file) {
            List<Day> days = new ArrayList<>();
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
                days.addAll((List<Day>) in.readObject());
            } catch (IOException | ClassNotFoundException e) {
                Logger.logError(LOG_TAG, e);
            }
            return days;
        }
    }
}
