package com.nsofta.newmusicmallplayer.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.nsofta.newmusicmallplayer.R;
import com.nsofta.newmusicmallplayer.eventbus.ChannelCheckedEvent;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.storage.StorageUriBuilder;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class ChannelsAdapter extends RecyclerView.Adapter<ChannelsAdapter.ChannelsViewHolder> {

    public static final String LOG_TAG = ChannelsAdapter.class.getName();
    private List<Channel> mChannels;
    private int checkedChannelsCounter;
    private Context context;

    public ChannelsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ChannelsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChannelsViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item_channel, parent, false));
    }

    @Override
    public void onBindViewHolder(final ChannelsViewHolder holder, int position) {
        final Channel channel = mChannels.get(holder.getAdapterPosition());

        if (!TextUtils.isEmpty(channel.color))
            holder.itemView.setBackgroundColor(Color.parseColor(channel.color));

        holder.checkBox.setChecked(channel.checked);
        holder.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!PreferenceUtils.isCorporateEdition(context)) {
                if (isChecked && checkedChannelsCounter == 5) {
                    buttonView.setChecked(false);
                    Toast.makeText(context,
                            context.getString(R.string.error_many_channels, 5), Toast.LENGTH_LONG).show();
                } else if (isChecked && checkedChannelsCounter < 5) {
                    checkedChannelsCounter++;
                    channel.checked = true;
                    EventBus.getDefault().post(new ChannelCheckedEvent(channel.id, channel.checked));
                } else {
                    checkedChannelsCounter--;
                    channel.checked = false;
                    EventBus.getDefault().post(new ChannelCheckedEvent(channel.id, channel.checked));
                }
            } else {
                Toast.makeText(context, "Эта функция не доступна в Corporate Edition.",
                        Toast.LENGTH_LONG).show();
            }
        });

        holder.info.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(channel.description))
                new MaterialDialog.Builder(context)
                        .title(channel.name)
                        .titleColor(ContextCompat.getColor(context, R.color.colorAccent))
                        .content(channel.description)
                        .positiveText("OK")
                        .positiveColor(ContextCompat.getColor(context, R.color.colorAccent))
                        .build().show();
        });
        Uri uri = ChannelsViewHolder.parentUri
                .appendFile(channel.img + ".png")
                .getFile()
                .getUri();
        Glide.with(context)
                .loadFromMediaStore(uri)
                .into(holder.logoImageView);
        holder.titleTextView.setText(channel.name);
    }

    @Override
    public int getItemCount() {
        return mChannels == null ? 0 : mChannels.size();
    }

    public void setChannels(List<Channel> channels) {
        mChannels = channels;
        this.notifyDataSetChanged();
    }

    static class ChannelsViewHolder extends RecyclerView.ViewHolder {

        static StorageUriBuilder.DirUri parentUri;
        TextView titleTextView;
        ImageView logoImageView;
        CheckBox checkBox;
        ImageButton info;

        ChannelsViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.txt_title);
            logoImageView = (ImageView) itemView.findViewById(R.id.img_preview);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
            info = (ImageButton) itemView.findViewById(R.id.chnInfo);
            parentUri = StorageUriBuilder.fromDirUri(logoImageView.getContext(),
                    Uri.parse(PreferenceUtils.getSdPath(logoImageView.getContext())))
                    .appendDirectory("MusicMall")
                    .appendDirectory("img");
        }
    }

}
