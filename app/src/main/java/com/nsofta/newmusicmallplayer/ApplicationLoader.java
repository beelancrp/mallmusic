package com.nsofta.newmusicmallplayer;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import io.fabric.sdk.android.Fabric;

public class ApplicationLoader extends Application {

    private static ApplicationLoader instance;

    public static ApplicationLoader getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        HelperFactory.setHelper(getApplicationContext());
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        HelperFactory.releaseHelper();
    }

}
