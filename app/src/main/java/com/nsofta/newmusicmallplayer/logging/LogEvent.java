package com.nsofta.newmusicmallplayer.logging;

public class LogEvent {

    public static final int TRACK_FINISHED = 0;
    public static final int FTP_LOADED = 1;
    public static final int USER_ACTION = 2;
    public static final int DEVICE_STATE_CHANGED = 3;
    public static final int CHANNEL_STATE_CHANGED = 4;
    public static final int ERROR = 5;

}
