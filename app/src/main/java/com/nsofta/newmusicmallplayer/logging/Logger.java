package com.nsofta.newmusicmallplayer.logging;

import android.util.Log;

import com.nsofta.newmusicmallplayer.BuildConfig;

public class Logger {

    public static void logError(String tag, Throwable t) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, t.toString());
        }
    }

}
