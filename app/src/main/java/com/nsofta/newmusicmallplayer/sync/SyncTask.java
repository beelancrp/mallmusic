package com.nsofta.newmusicmallplayer.sync;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nsofta.newmusicmallplayer.api.MallRestService;
import com.nsofta.newmusicmallplayer.api.model.AdResponse;
import com.nsofta.newmusicmallplayer.api.model.MusicResponse;
import com.nsofta.newmusicmallplayer.api.model.ObjectResponse;
import com.nsofta.newmusicmallplayer.database.ChannelsDAO;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.player.model.Day;
import com.nsofta.newmusicmallplayer.player.model.MallInfo;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;


public class SyncTask extends IntentService {

    public static final String LOG_TAG = SyncTask.class.getName();
    private Context mContext;
    private ChannelsDAO mChannelsDao;
    private int id;

    public SyncTask(String name) {
        super(name);
    }

    public SyncTask() {
        super(SyncTask.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("Push", "START SYNC SERVICE");
        mContext = getApplicationContext();
        if (intent != null) {
            if (intent.hasExtra("start_sync")) {
                id = PreferenceUtils.getId(mContext);
                Response<ObjectResponse> response;
                try {
                    response = MallRestService.getApi().getObject(id).execute();
                    if (response.isSuccessful() && response.body().invalidate()) {
                        MallInfo mallInfo = response.body().mallInfo;
                        PreferenceUtils.setName(mContext, mallInfo.name);
                        PreferenceUtils.setFTPAddress(mContext, mallInfo.ftpHost);
                        PreferenceUtils.setFTPLogin(mContext, mallInfo.ftpLogin);
                        PreferenceUtils.setFTPPassword(mContext, mallInfo.ftpPassword);
                        boolean corporateEdition = mallInfo.corporateEdition;
                        if (corporateEdition) {
                            saveDays(new ArrayList<>(mallInfo.days));
                        }
                        PreferenceUtils.setCorporateEdition(mContext, corporateEdition);
                        PreferenceUtils.setHasAds(mContext, mallInfo.useAds);
                        PreferenceUtils.setPlayAdFixed(mContext, mallInfo.advFixedTime);
                        loadMusic();
                        loadAds();
                    }
                } catch (IOException e) {
                    Logger.logError(LOG_TAG, e);
                }
            }
        }
    }

    private void loadMusic() throws IOException {
        Response<MusicResponse> response = MallRestService.getApi().getMusic(id).execute();
        if (response.isSuccessful() && response.body().invalidate()) {
            List<Channel> channels = response.body().channels;
            new LoadTracksTask(mContext).execute(channels.toArray(new Channel[channels.size()]));
        }
    }

    private void loadAds() throws IOException {
        Response<AdResponse> response = MallRestService.getApi().getAds(id).execute();
        if (response.isSuccessful() && response.body().invalidate()) {
            new LoadAdsTask(mContext).execute();
        }
    }

    private void saveDays(ArrayList<Day> days) {
        String path = mContext.getFilesDir().getPath() + "/" + PreferenceUtils.getDaysName(mContext);
        File file = new File(path);
        try {
            file.createNewFile();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(days);
            out.close();
        } catch (IOException e) {
            Logger.logError(LOG_TAG, e);
        }
    }
}
