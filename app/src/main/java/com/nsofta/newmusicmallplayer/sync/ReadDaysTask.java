package com.nsofta.newmusicmallplayer.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.Day;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class ReadDaysTask extends AsyncTask<Void, Void, List<Day>> {

    public static final String LOG_TAG = ReadDaysTask.class.getName();
    private Context mContext;

    public ReadDaysTask(Context context) {
        mContext = context;
    }

    @Override
    protected List<Day> doInBackground(Void... params) {
        String path = mContext.getFilesDir().getPath() + "/" + PreferenceUtils.getDaysName(mContext);
        File file = new File(path);
        if (file.exists() && file.length() > 0) {
            return readDays(file);
        }
        return new ArrayList<>();
    }

    private List<Day> readDays(File file) {
        List<Day> days = new ArrayList<>();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            days.addAll((List<Day>) in.readObject());
        } catch (IOException | ClassNotFoundException e) {
            Logger.logError(LOG_TAG, e);
        }
        return days;
    }
}
