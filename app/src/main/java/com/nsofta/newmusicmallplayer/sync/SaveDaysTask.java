package com.nsofta.newmusicmallplayer.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.Day;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class SaveDaysTask extends AsyncTask<Day, Void, Boolean> {

    public static final String LOG_TAG = SaveDaysTask.class.getName();
    private Context context;

    public SaveDaysTask(Context context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Day... params) {
        saveDays(new ArrayList<>(Arrays.asList(params)));
        return true;
    }

    private void saveDays(ArrayList<Day> days) {
        String path = context.getFilesDir().getPath() + "/" + PreferenceUtils.getDaysName(context);
        File file = new File(path);
        try {
            file.createNewFile();
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(days);
            out.close();
            Log.d(LOG_TAG, "Today file saved");
        } catch (IOException e) {
            Logger.logError(LOG_TAG, e);
        }
    }
}
