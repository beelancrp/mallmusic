package com.nsofta.newmusicmallplayer.sync;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.provider.DocumentFile;
import android.util.Log;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.nsofta.newmusicmallplayer.database.AdsDAO;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.ftp.FTPDirectory;
import com.nsofta.newmusicmallplayer.ftp.FTPManager;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.Ad;
import com.nsofta.newmusicmallplayer.storage.CryptoManager;
import com.nsofta.newmusicmallplayer.storage.StorageUriBuilder;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LoadAdsTask extends AsyncTask<Void, Ad, List<Ad>> {

    public static final String LOG_TAG = LoadAdsTask.class.getName();

    private Context mContext;
    private FTPManager mFtpManager;
    private AdsDAO mAdsDAO;
    private CryptoManager mCryptoManager;

    public LoadAdsTask(Context context) {
        this.mContext = context;
        this.mFtpManager = new FTPManager();
        try {
            mAdsDAO = HelperFactory.getHelper().getAdsDao();
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);

        }
        mCryptoManager = CryptoManager.getInstance(context);
    }

    @Override
    protected List<Ad> doInBackground(Void... params) {
        mFtpManager.connect();

        clearDir(getAdsDir());

        loadAds();

        return getGoodAds();
    }

    private List<Ad> getGoodAds() {
        List<Ad> ads = new ArrayList<>();
        try {
            ads.addAll(mAdsDAO.queryForAll());
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);
        }
        return ads;
    }

    private void loadAds() {
        FTPDirectory dir = FTPDirectory.getAdDirectory();
        mFtpManager.changeDirectory(dir.getPath());
        List<String> names = mFtpManager.listFiles();
        for (String name : names) {
            if (adsExists(name)) {
                StorageUriBuilder.FileUri adUri = StorageUriBuilder.fromFile(getAdsFile(name));
                try {
                    mFtpManager.downloadFile(adUri.getOutputStream(mContext), name);
                    DocumentFile encryptedFile = mCryptoManager.encrypt(name, adUri.getFile());
                    if (encryptedFile != null) {
                        Ad ad = new Ad();
                        ad.adName = name.replace("\'", "");
                        ad.filePath = encryptedFile.getUri().getPath();
                        mAdsDAO.create(ad);
                        publishProgress(ad);
                        adUri.delete();
                    }
                } catch (FileNotFoundException | SQLException e) {
                    Logger.logError(LOG_TAG, e);
                }
            }
        }
    }

    private boolean adsExists(String title) {
        List<Ad> ads = new ArrayList<>();
        try {
            ads.addAll(mAdsDAO.queryForAll());
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);
            return false;
        }

        for (Ad ad : ads) {
            if (title.equals(ad.adName))
                return true;
        }

        return false;
    }

    private void clearDir(DocumentFile parent) {
        for (DocumentFile doc : parent.listFiles()) {
            try {
                DeleteBuilder<Ad, Integer> builder = mAdsDAO.deleteBuilder();
                builder.where().eq(Ad.Columns.PATH, doc.getUri().toString());
                int delete = mAdsDAO.delete(builder.prepare());
                Log.d(LOG_TAG, "Deleted: " + delete);
            } catch (SQLException e) {
                Logger.logError(LOG_TAG, e);
            }
            if (!doc.isDirectory()) doc.delete();
        }
    }

    private DocumentFile getAdsDir() {
        return StorageUriBuilder.fromDirUri(mContext, Uri.parse(PreferenceUtils.getSdPath(mContext)))
                .appendDirectory("MusicMall")
                .appendDirectory("Adv")
                .getFile();
    }

    private DocumentFile getAdsFile(String adName) {
        return StorageUriBuilder.fromDir(getAdsDir())
                .appendFile(adName)
                .getFile();
    }


}
