package com.nsofta.newmusicmallplayer.sync;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.provider.DocumentFile;
import android.text.TextUtils;
import android.util.Log;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.nsofta.newmusicmallplayer.database.ChannelsDAO;
import com.nsofta.newmusicmallplayer.database.HelperFactory;
import com.nsofta.newmusicmallplayer.database.TracksDAO;
import com.nsofta.newmusicmallplayer.ftp.FTPDirectory;
import com.nsofta.newmusicmallplayer.ftp.FTPManager;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.player.model.BlockedTrack;
import com.nsofta.newmusicmallplayer.player.model.Channel;
import com.nsofta.newmusicmallplayer.player.model.Track;
import com.nsofta.newmusicmallplayer.storage.CryptoManager;
import com.nsofta.newmusicmallplayer.storage.StorageUriBuilder;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Грузит треки с фпт и шифрует. Данные заносит в БД. При этом отвечает за очистку предыдущих
 * директорий.
 */
public class LoadTracksTask extends AsyncTask<Channel, Channel, List<Track>> {

    public static final String LOG_TAG = LoadTracksTask.class.getName();

    private Context mContext;
    private FTPManager mFtpManager;
    private TracksDAO mTracksDao;
    private ChannelsDAO mChannelsDao;
    private CryptoManager mCryptoManager;

    public LoadTracksTask(Context context) {
        mContext = context;
        mFtpManager = new FTPManager();
        try {
            mTracksDao = HelperFactory.getHelper().getTracksDao();
            mChannelsDao = HelperFactory.getHelper().getChannelsDao();
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);
        }
        mCryptoManager = CryptoManager.getInstance(mContext);
    }

    @Override
    protected List<Track> doInBackground(Channel... channels) {
        mFtpManager.connect();
        for (Channel ch : channels) {
            int maxIndex = findMaxIndex(ch);
            if (ch.index == 0 || ch.index == maxIndex) {
                ch.index = 1;
            } else {
                ch.index++;
            }
            for (int i = 1; i < maxIndex; i++) {
                clearDir(getChannelsDir(ch, i));
            }
            loadChannel(ch);
        }
        return getGoodTracks();
    }

    private List<Track> getGoodTracks() {
        List<Track> allTracks = new ArrayList<>();
        try {
            List<Channel> goodChannels = mChannelsDao.queryBuilder()
                    .where()
                    .gt(Channel.Columns.RATIO, "0")
                    .query();
            for (Channel ch : goodChannels) {
                allTracks.addAll(mTracksDao.queryBuilder()
                        .where().eq(Track.Columns.CHANNEL, ch.id)
                        .query());
            }
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);
        }
        return allTracks;
    }

    private void loadChannel(Channel channel) {
        FTPDirectory dir = FTPDirectory.getChannelDirectory(channel.name, channel.index);
        mFtpManager.changeDirectory(dir.getPath());
        List<String> names = mFtpManager.listFiles();
        for (int i = 0; i < names.size(); i++) {
            String n = names.get(i);
            if (trackExists(n)) continue;
            StorageUriBuilder.FileUri trackUri = StorageUriBuilder.fromFile(getTrackFile(channel, n));
            try {
                mFtpManager.downloadFile(trackUri.getOutputStream(mContext), n);
                DocumentFile encryptedFile = mCryptoManager.encrypt(n, trackUri.getFile());
                if (encryptedFile != null) {
                    Track track = new Track();
                    track.channel = channel;
                    track.path = encryptedFile.getUri().toString();
                    track.title = n.replace("\'", "");
                    track.lengthMillis = retrieveTrackLength(trackUri.getFile());
                    mTracksDao.create(track);
                    trackUri.delete();
                }
            } catch (FileNotFoundException | SQLException e) {
                Logger.logError(LOG_TAG, e);
            }
        }
        publishProgress(channel);
    }

    private boolean trackExists(String title) {
        List<Track> tracks = new ArrayList<>();
        List<BlockedTrack> blockedTracks = new ArrayList<>();
        try {
            blockedTracks.addAll(HelperFactory.getHelper().getBlockedTracksDao().queryForAll());
            tracks.addAll(mTracksDao.queryForEq(Track.Columns.TITLE, title.replace("\'", "")));
        } catch (SQLException e) {
            Logger.logError(LOG_TAG, e);
            return false;
        }
        for (BlockedTrack bt : blockedTracks) {
            if (title.equals(bt.fileName)) {
                return true;
            }
        }
        if (tracks.isEmpty()) return false;
        if (TextUtils.isEmpty(tracks.get(0).path)) return false;

        DocumentFile encryptedFile = StorageUriBuilder
                .fromFileUri(mContext, Uri.parse(tracks.get(0).path)).getFile();
        return encryptedFile.exists() && encryptedFile.length() > 0;
    }

    private int retrieveTrackLength(DocumentFile trackFile) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(mContext, trackFile.getUri());
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        if (time == null)
            return 0;
        else
            return Integer.parseInt(time);
    }

    private void clearDir(DocumentFile parent) {
        for (DocumentFile doc : parent.listFiles()) {
            try {
                DeleteBuilder<Track, Integer> builder = mTracksDao.deleteBuilder();
                builder.where().eq(Track.Columns.PATH, doc.getUri().toString());
                int delete = mTracksDao.delete(builder.prepare());
                Log.d(LOG_TAG, "Deleted: " + delete);
            } catch (SQLException e) {
                Logger.logError(LOG_TAG, e);
            }
            if (!doc.isDirectory()) doc.delete();
        }
        parent.delete();
    }

    private DocumentFile getChannelsDir(Channel channel) {
        return StorageUriBuilder.fromDirUri(mContext, Uri.parse(PreferenceUtils.getSdPath(mContext)))
                .appendDirectory("MusicMall")
                .appendDirectory("Music")
                .appendDirectory(channel.name)
                .appendDirectory(Integer.toString(channel.index))
                .getFile();
    }

    private DocumentFile getChannelsDir(Channel channel, int index) {
        return StorageUriBuilder.fromDirUri(mContext, Uri.parse(PreferenceUtils.getSdPath(mContext)))
                .appendDirectory("MusicMall")
                .appendDirectory("Music")
                .appendDirectory(channel.name)
                .appendDirectory(Integer.toString(index))
                .getFile();
    }

    private DocumentFile getTrackFile(Channel channel, String track) {
        return StorageUriBuilder.fromDir(getChannelsDir(channel))
                .appendFile(track)
                .getFile();
    }

    private int findMaxIndex(Channel channel) {
        mFtpManager.changeDirectory(FTPDirectory.getChannelRootDirectory(channel.name).getPath());
        List<String> list = mFtpManager.listDirectores();
        Collections.sort(list, (o1, o2) -> {
            try {
                Integer index1 = Integer.parseInt(o1);
                Integer index2 = Integer.parseInt(o2);
                return index1.compareTo(index2);
            } catch (NumberFormatException e) {
                Log.e(LOG_TAG, e.toString());
                return 0;
            }
        });
        return Integer.parseInt(list.get(list.size() - 1));
    }
}
