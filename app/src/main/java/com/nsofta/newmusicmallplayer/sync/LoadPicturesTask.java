package com.nsofta.newmusicmallplayer.sync;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import com.nsofta.newmusicmallplayer.ftp.FTPDirectory;
import com.nsofta.newmusicmallplayer.ftp.FTPManager;
import com.nsofta.newmusicmallplayer.logging.Logger;
import com.nsofta.newmusicmallplayer.storage.StorageUriBuilder;
import com.nsofta.newmusicmallplayer.utils.PreferenceUtils;

import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.List;

public class LoadPicturesTask extends AsyncTask<Void, StorageUriBuilder.FileUri, Boolean> {

    public static final String LOG_TAG = LoadPicturesTask.class.getName();
    private Context mContext;

    public LoadPicturesTask(Context context) {
        mContext = context;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        String sdPath = PreferenceUtils.getSdPath(mContext);
        FTPDirectory picsDir = FTPDirectory.getImgDirectory();
        FTPManager ftpManager = new FTPManager();
        ftpManager.connect();
        ftpManager.changeDirectory(picsDir.getPath());
        List<String> files = ftpManager.listFiles();
        for (String pic : files) {
            try {
                StorageUriBuilder.FileUri picUri = StorageUriBuilder.fromDirUri(mContext,
                        Uri.parse(sdPath))
                        .appendDirectory("MusicMall")
                        .appendDirectory("img")
                        .appendFile(pic);
                if (picUri.getFile().length() == 0) {
                    OutputStream outputStream = picUri.getOutputStream(mContext);
                    ftpManager.downloadFile(outputStream, pic);
                    publishProgress(picUri);
                }
            } catch (FileNotFoundException e) {
                Logger.logError(LOG_TAG, e);
            }
        }
        ftpManager.release();
        return true;
    }
}
